package mc.avalon.api;

import lombok.Getter;

public class API {
	
	/**
	 * Return API instance.
	 */
	
	@Getter private static API instance;
	
	/**
	 * @Constructor 
	 * 
	 *  Instancing API.
	 */
	
	public API() {
		instance = this;
	}

}
